package br.edu.uniateneu.sgpp.sgpp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
@EntityScan("br.edu.uniateneu.sgpp.model*")
@SpringBootApplication
public class SgppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SgppApplication.class, args);
	}

}
