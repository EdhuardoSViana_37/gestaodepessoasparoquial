package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_CelebraPrimeiraEucaristia")

public class CelebracaoPrimeiraEucaristiaEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_CelebraPrimeiraEucaristia")
	long idPrimeiraEucaristia;
	
	@Column(name = "NomeCatequizando")
	private String nomeCatequizando;
	
	@Column(name = "dataCelebracao_PrimeiraEucaristia")
	private String dataCelebracao_PrimeiraEucaristia;
	
	@Column(name = "localCelebracao_PrimeiraEucaristia")
	private String localCelebracao_PrimeiraEucaristia;
	
	@Column(name = "horarioCelebracao_PrimeiraEucaristia")
	private String horarioCelebracao_PrimeiraEucaristia;
	
	@Column(name = "celebrante_PrimeiraEucaristia")
	private String celebrantePrimeiraEucaristia;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity pessoa;
	
	public long getIdPrimeiraEucaristia() {
		return idPrimeiraEucaristia;
	}

	public void setIdPrimeiraEucaristia(long idPrimeiraEucaristia) {
		this.idPrimeiraEucaristia = idPrimeiraEucaristia;
	}

	public String getNomeCatequizando() {
		return nomeCatequizando;
	}

	public void setNomeCatequizando(String nomeCatequizando) {
		this.nomeCatequizando = nomeCatequizando;
	}

	public String getDataCelebracao_PrimeiraEucaristia() {
		return dataCelebracao_PrimeiraEucaristia;
	}

	public void setDataCelebracao_PrimeiraEucaristia(String dataCelebracao_PrimeiraEucaristia) {
		this.dataCelebracao_PrimeiraEucaristia = dataCelebracao_PrimeiraEucaristia;
	}

	public String getLocalCelebracao_PrimeiraEucaristia() {
		return localCelebracao_PrimeiraEucaristia;
	}

	public void setLocalCelebracao_PrimeiraEucaristia(String localCelebracao_PrimeiraEucaristia) {
		this.localCelebracao_PrimeiraEucaristia = localCelebracao_PrimeiraEucaristia;
	}

	public String getHorarioCelebracao_PrimeiraEucaristia() {
		return horarioCelebracao_PrimeiraEucaristia;
	}

	public void setHorarioCelebracao_PrimeiraEucaristia(String horarioCelebracao_PrimeiraEucaristia) {
		this.horarioCelebracao_PrimeiraEucaristia = horarioCelebracao_PrimeiraEucaristia;
	}

	public String getCelebrantePrimeiraEucaristia() {
		return celebrantePrimeiraEucaristia;
	}

	public void setCelebrantePrimeiraEucaristia(String celebrantePrimeiraEucaristia) {
		this.celebrantePrimeiraEucaristia = celebrantePrimeiraEucaristia;
	}
}