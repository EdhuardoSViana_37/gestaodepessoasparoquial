package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_CelebraCrisma")

public class CelebracaoCrismaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_CelebraCrisma")
	long idCelebraCrisma;
	
	@Column(name = "NomeCrismando")
	private String nomeCrismando;
	
	@Column(name = "NomePadrinho")
	private String nomePadrinho;
	
	@Column(name = "dataCelebracao_Crisma")
	private String dataCelebracao_Crisma;
	
	@Column(name = "localCelebracao_Crisma")
	private String localCelebracao_Crisma;
	
	@Column(name = "horarioCelebracao_Crisma")
	private String horarioCelebracao_Crisma;
	
	@Column(name = "celebrante_Crisma")
	private String celebranteCrisma;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity pessoa;
	
	public long getIdCelebraCrisma() {
		return idCelebraCrisma;
	}

	public void setIdCelebraCrisma(long idCelebraCrisma) {
		this.idCelebraCrisma = idCelebraCrisma;
	}

	public String getNomeCrismando() {
		return nomeCrismando;
	}

	public void setNomeCrismando(String nomeCrismando) {
		this.nomeCrismando = nomeCrismando;
	}

	public String getNomePadrinho() {
		return nomePadrinho;
	}

	public void setNomePadrinho(String nomePadrinho) {
		this.nomePadrinho = nomePadrinho;
	}

	public String getDataCelebracao_Crisma() {
		return dataCelebracao_Crisma;
	}

	public void setDataCelebracao_Crisma(String dataCelebracao_Crisma) {
		this.dataCelebracao_Crisma = dataCelebracao_Crisma;
	}

	public String getLocalCelebracao_Crisma() {
		return localCelebracao_Crisma;
	}

	public void setLocalCelebracao_Crisma(String localCelebracao_Crisma) {
		this.localCelebracao_Crisma = localCelebracao_Crisma;
	}

	public String getHorarioCelebracao_Crisma() {
		return horarioCelebracao_Crisma;
	}

	public void setHorarioCelebracao_Crisma(String horarioCelebracao_Crisma) {
		this.horarioCelebracao_Crisma = horarioCelebracao_Crisma;
	}

	public String getCelebranteCrisma() {
		return celebranteCrisma;
	}

	public void setCelebranteCrisma(String celebranteCrisma) {
		this.celebranteCrisma = celebranteCrisma;
	}
}