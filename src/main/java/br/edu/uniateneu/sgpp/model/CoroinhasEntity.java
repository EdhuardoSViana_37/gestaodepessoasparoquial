package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_Coroinhas")

public class CoroinhasEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Coroinhas")
	long idCoroinhas;

	@Column(name = "diaReuniao_Coroinhas")
	private String diaReunioes;

	@Column(name = "horarioReuniao_Coroinhas")
	private String horarioReunioes;

	@Column(name = "nomeCoordenador")
	private String nomeCoordenador;

	@Column(name = "escala_Coroinhas")
	private String escalaCoroinhas;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity acolito;
	
	public long getIdCoroinhas() {
		return idCoroinhas;
	}

	public void setIdCoroinhas(long idCoroinhas) {
		this.idCoroinhas = idCoroinhas;
	}

	public String getDiaReunioes() {
		return diaReunioes;
	}

	public void setDiaReunioes(String diaReunioes) {
		this.diaReunioes = diaReunioes;
	}

	public String getHorarioReunioes() {
		return horarioReunioes;
	}

	public void setHorarioReunioes(String horarioReunioes) {
		this.horarioReunioes = horarioReunioes;
	}

	public String getNomeCoordenador() {
		return nomeCoordenador;
	}

	public void setNomeCoordenador(String nomeCoordenador) {
		this.nomeCoordenador = nomeCoordenador;
	}

	public String getEscalaCoroinhas() {
		return escalaCoroinhas;
	}

	public void setEscalaCoroinhas(String escalaCoroinhas) {
		this.escalaCoroinhas = escalaCoroinhas;
	}
}