package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_Dizimo")

public class DizimoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Dizimo")
	long idDizimo;
	
	@Column(name = "dataVencimento")
	private String dataVencimento;
	
	@Column(name = "valorContribuicao")
	private float valorContribuicao;
	
	@Column(name = "tipoContribuicao")
	private String tipoContribuicao;
	
	@Column(name = "formaPagamento")
	private String formaPagamento;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity dizimista;
	
	public long getIdDizimo() {
		return idDizimo;
	}

	public void setIdDizimo(long idDizimo) {
		this.idDizimo = idDizimo;
	}

	public String getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public float getValorContribuicao() {
		return valorContribuicao;
	}

	public void setValorContribuicao(float valorContribuicao) {
		this.valorContribuicao = valorContribuicao;
	}

	public String getTipoContribuicao() {
		return tipoContribuicao;
	}

	public void setTipoContribuicao(String tipoContribuicao) {
		this.tipoContribuicao = tipoContribuicao;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
}