package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_Crisma")

public class CrismaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Crisma")
	long idCrisma;

	@Column(name = "diaReuniao_Crisma")
	private String diaReunioes;

	@Column(name = "horarioReuniao_Crisma")
	private String horarioReunioes;

	@Column(name = "nomeCatequista")
	private String nomeCatequista;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity participante;
	
	public long getIdCrisma() {
		return idCrisma;
	}

	public void setIdCrisma(long idCrisma) {
		this.idCrisma = idCrisma;
	}

	public String getDiaReunioes() {
		return diaReunioes;
	}

	public void setDiaReunioes(String diaReunioes) {
		this.diaReunioes = diaReunioes;
	}

	public String getHorarioReunioes() {
		return horarioReunioes;
	}

	public void setHorarioReunioes(String horarioReunioes) {
		this.horarioReunioes = horarioReunioes;
	}

	public String getNomeCatequista() {
		return nomeCatequista;
	}

	public void setNomeCatequista(String nomeCatequista) {
		this.nomeCatequista = nomeCatequista;
	}
}