package br.edu.uniateneu.sgpp.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_CelebraBatismo")

public class CelebracaoBatismoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_CelebraBatismo")
	long idCelebraBatismo;
	
	@Column(name = "NomeBatizando")
	private String nomeBatizando;
	
	@Column(name = "NomedoPai")
	private String nomePai;
	
	@Column(name = "NomedaMae")
	private String nomeMae;
	
	@Column(name = "NomePadrinho")
	private String nomePadrinho;
	
	@Column(name = "NomeMadrinha")
	private String nomeMadrinha;
	
	@Column(name = "dataCelebracao_Batismo")
	private String dataCelebracao_Batismo;
	
	@Column(name = "localCelebracao_Batismo")
	private String localCelebracao_Batismo;
	
	@Column(name = "horarioCelebracao_Batismo")
	private String horarioCelebracao_Batismo;
	
	@Column(name = "celebrante_Batismo")
	private String celebranteBatismo;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity pessoa;
	
	public long getIdCelebraBatismo() {
		return idCelebraBatismo;
	}

	public void setIdCelebraBatismo(long idCelebraBatismo) {
		this.idCelebraBatismo = idCelebraBatismo;
	}

	
	public String getNomeBatizando() {
		return nomeBatizando;
	}

	public void setNomeBatizando(String nomeBatizando) {
		this.nomeBatizando = nomeBatizando;
	}

	public String getNomePai() {
		return nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getNomePadrinho() {
		return nomePadrinho;
	}

	public void setNomePadrinho(String nomePadrinho) {
		this.nomePadrinho = nomePadrinho;
	}

	public String getNomeMadrinha() {
		return nomeMadrinha;
	}

	public void setNomeMadrinha(String nomeMadrinha) {
		this.nomeMadrinha = nomeMadrinha;
	}

	public String getDataCelebracao_Batismo() {
		return dataCelebracao_Batismo;
	}

	public void setDataCelebracao_Batismo(String dataCelebracao_Batismo) {
		this.dataCelebracao_Batismo = dataCelebracao_Batismo;
	}

	public String getLocalCelebracao_Batismo() {
		return localCelebracao_Batismo;
	}

	public void setLocalCelebracao_Batismo(String localCelebracao_Batismo) {
		this.localCelebracao_Batismo = localCelebracao_Batismo;
	}

	public String getHorarioCelebracao_Batismo() {
		return horarioCelebracao_Batismo;
	}

	public void setHorarioCelebracao_Batismo(String horarioCelebracao_Batismo) {
		this.horarioCelebracao_Batismo = horarioCelebracao_Batismo;
	}

	public String getCelebranteBatismo() {
		return celebranteBatismo;
	}

	public void setCelebranteBatismo(String celebranteBatismo) {
		this.celebranteBatismo = celebranteBatismo;
	}
}