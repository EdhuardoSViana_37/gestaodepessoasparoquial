package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_Batismo")

public class BatismoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Batismo")
	long idBatismo;

	@Column(name = "diaReuniao_Batismo")
	private String diaReunioes;

	@Column(name = "horarioReuniao_Batismo")
	private String horarioReunioes;

	@Column(name = "nomeCoordenador")
	private String nomeCoordenador;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity participante;
	
	public long getIdBatismo() {
		return idBatismo;
	}

	public void setIdBatismo(long idBatismo) {
		this.idBatismo = idBatismo;
	}

	public String getDiaReunioes() {
		return diaReunioes;
	}

	public void setDiaReunioes(String diaReunioes) {
		this.diaReunioes = diaReunioes;
	}

	public String getHorarioReunioes() {
		return horarioReunioes;
	}

	public void setHorarioReunioes(String horarioReunioes) {
		this.horarioReunioes = horarioReunioes;
	}

	public String getNomeCoordenador() {
		return nomeCoordenador;
	}

	public void setNomeCatequista(String nomeCoordenador) {
		this.nomeCoordenador = nomeCoordenador;
	}
}