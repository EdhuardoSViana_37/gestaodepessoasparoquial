package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_Matrimonio")

public class MatrimonioEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Matrimonio")
	long idMatrimonio;
	
	@Column(name = "diaReuniao_Matrimonio")
	private String diaReunioes;

	@Column(name = "horarioReuniao_Matrimonio")
	private String horarioReunioes;

	@Column(name = "nomeCoordenador")
	private String nomeCoordenador;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity participante;
	
	public long getIdMatrimonio() {
		return idMatrimonio;
	}
	
	public void setIdMatrimonio(long idMatrimonio) {
		this.idMatrimonio = idMatrimonio;
	}

	public String getDiaReunioes() {
		return diaReunioes;
	}

	public void setDiaReunioes(String diaReunioes) {
		this.diaReunioes = diaReunioes;
	}

	public String getHorarioReunioes() {
		return horarioReunioes;
	}

	public void setHorarioReunioes(String horarioReunioes) {
		this.horarioReunioes = horarioReunioes;
	}

	public String getNomeCoordenador() {
		return nomeCoordenador;
	}

	public void setNomeCoordenador(String nomeCoordenador) {
		this.nomeCoordenador = nomeCoordenador;
	}	
}