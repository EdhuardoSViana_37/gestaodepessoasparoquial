package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_Religioso")

public class ReligiosoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Religioso")
	long idReligioso;
	
	@Column(name = "nome_Religioso")
	private String nome;
	
	@ManyToOne
	private ParoquiaEntity paroquia;
	
	@Column(name = "dataDeNascimento_Religioso")
	private String dataNascimento;
	
	@Column(name = "dataDeOrdenacao_Religioso")
	private String dataOrdenacao;
	
	@Column(name = "cargo_Religioso")
	private String cargoReligioso;

	public long getIdReligioso() {
		return idReligioso;
	}

	public void setIdReligioso(long idReligioso) {
		this.idReligioso = idReligioso;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getDataOrdenacao() {
		return dataOrdenacao;
	}

	public void setDataOrdenacao(String dataOrdenacao) {
		this.dataOrdenacao = dataOrdenacao;
	}

	public String getCargoReligioso() {
		return cargoReligioso;
	}

	public void setCargoReligioso(String cargoReligioso) {
		this.cargoReligioso = cargoReligioso;
	}
}