package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_CelebraMatrimonio")

public class CelebracaoMatrimonioEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_CelebraMatrimonio")
	long idCelebraMatrimonio;
	
	@Column(name = "NomeDoNubente")
	private String nomeDoNubente;
	
	@Column(name = "NomeDaNubente")
	private String nomeDaNubente;
	
	@Column(name = "dataCelebracao_Matrimonio")
	private String dataCelebracao_Matrimonio;
	
	@Column(name = "localCelebracao_Matrimonio")
	private String localCelebracao_Matrimonio;
	
	@Column(name = "horarioCelebracao_Matrimonio")
	private String horarioCelebracao_Matrimonio;
	
	@Column(name = "celebrante_Matrimonio")
	private String celebranteMatrimonio;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity pessoa;
	
	public long getIdCelebraMatrimonio() {
		return idCelebraMatrimonio;
	}

	public void setIdCelebraMatrimonio(long idCelebraMatrimonio) {
		this.idCelebraMatrimonio = idCelebraMatrimonio;
	}

	public String getNomeDoNubente() {
		return nomeDoNubente;
	}

	public void setNomeDoNubente(String nomeDoNubente) {
		this.nomeDoNubente = nomeDoNubente;
	}

	public String getNomeDaNubente() {
		return nomeDaNubente;
	}

	public void setNomeDaNubente(String nomeDaNubente) {
		this.nomeDaNubente = nomeDaNubente;
	}

	public String getDataCelebracao_Matrimonio() {
		return dataCelebracao_Matrimonio;
	}

	public void setDataCelebracao_Matrimonio(String dataCelebracao_Matrimonio) {
		this.dataCelebracao_Matrimonio = dataCelebracao_Matrimonio;
	}

	public String getLocalCelebracao_Matrimonio() {
		return localCelebracao_Matrimonio;
	}

	public void setLocalCelebracao_Matrimonio(String localCelebracao_Matrimonio) {
		this.localCelebracao_Matrimonio = localCelebracao_Matrimonio;
	}

	public String getHorarioCelebracao_Matrimonio() {
		return horarioCelebracao_Matrimonio;
	}

	public void setHorarioCelebracao_Matrimonio(String horarioCelebracao_Matrimonio) {
		this.horarioCelebracao_Matrimonio = horarioCelebracao_Matrimonio;
	}

	public String getCelebranteMatrimonio() {
		return celebranteMatrimonio;
	}

	public void setCelebranteMatrimonio(String celebranteMatrimonio) {
		this.celebranteMatrimonio = celebranteMatrimonio;
	}
}