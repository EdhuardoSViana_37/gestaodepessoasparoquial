package br.edu.uniateneu.sgpp.model;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_Pessoa")

public class PessoaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Pessoa")
	long idPessoa;

	@Column(name = "nome_Pessoa")
	private String nome;

	@Column(name = "dataNascimento_Pessoa")
	private String dataDeNascimento;

	@Column(name = "sexo_Pessoa")
	private String sexo;

	@Column(name = "end_Pessoa")
	private String endereco;

	@Column(name = "tel_Pessoa")
	private String telefone;

	@Column(name = "estadoCivil_Pessoa")
	private String estadoCivil;

	@Column(name = "mail_Pessoa")
	private String email;

	@Column(name = "rg_Pessoa")
	private String RG;

	@Column(name = "cpf_Pessoa")
	private String CPF;

	@Column(name = "funcao_Pessoa")
	private String funcaoParoquia;

	
	@OneToMany(mappedBy = "pessoa")
	private List<CelebracaoBatismoEntity> batismos;
	
	@OneToMany(mappedBy = "participante")
	private List<BatismoEntity> reuniao_batismo;
	
	@OneToMany(mappedBy = "pessoa")
	private List<CelebracaoCrismaEntity> crismas;
	
	@OneToMany(mappedBy = "participante")
	private List<CrismaEntity> reuniao_crisma;
	
	@OneToMany(mappedBy = "pessoa")
	private List<CelebracaoMatrimonioEntity> casamentos;
	
	@OneToMany(mappedBy = "participante")
	private List<MatrimonioEntity> reuniao_noivos;
	
	@OneToMany(mappedBy = "pessoa")
	private List<CelebracaoPrimeiraEucaristiaEntity> primeirascomunhoes;
	
	@OneToMany(mappedBy = "participante")
	private List<EucaristiaEntity> reuniao_eucaristia;
	
	@OneToMany(mappedBy = "acolito")
	private List<CoroinhasEntity> reuniao_coroinhas;
	
	@OneToMany(mappedBy = "dizimista")
	private List<DizimoEntity> dizimistas;
	
	@OneToMany(mappedBy = "ministros")
	private List<MESCEntity> reuniao_ministros;
	
	@OneToMany(mappedBy = "participante")
	private List<MovimentosEntity> reuniao_movimentos;
	
	
	public long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataDeNascimento() {
		return dataDeNascimento;
	}

	public void setDataDeNascimento(String dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRG() {
		return RG;
	}

	public void setRG(String rG) {
		RG = rG;
	}

	public String getCPF() {
		return CPF;
	}

	public void setCPF(String cPF) {
		CPF = cPF;
	}

	public String getFuncaoParoquia() {
		return funcaoParoquia;
	}

	public void setFuncaoParoquia(String funcaoParoquia) {
		this.funcaoParoquia = funcaoParoquia;
	}
}