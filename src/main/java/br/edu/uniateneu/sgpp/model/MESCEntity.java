package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_MESC")

public class MESCEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_MESC")
	long idMESC;

	@Column(name = "diaReuniao_MESC")
	private String diaReunioes;

	@Column(name = "horarioReuniao_MESC")
	private String horarioReunioes;

	@Column(name = "nomeCoordenador")
	private String nomeCoordenador;

	@Column(name = "escalaMinistros_MESC")
	private String escalaMinistros;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity ministros;
	
	public long getIdMESC() {
		return idMESC;
	}

	public void setIdMESC(long idMESC) {
		this.idMESC = idMESC;
	}

	public String getDiaReunioes() {
		return diaReunioes;
	}

	public void setDiaReunioes(String diaReunioes) {
		this.diaReunioes = diaReunioes;
	}

	public String getHorarioReunioes() {
		return horarioReunioes;
	}

	public void setHorarioReunioes(String horarioReunioes) {
		this.horarioReunioes = horarioReunioes;
	}

	public String getNomeCoordenador() {
		return nomeCoordenador;
	}

	public void setNomeCoordenador(String nomeCoordenador) {
		this.nomeCoordenador = nomeCoordenador;
	}

	public String getEscalaMinistros() {
		return escalaMinistros;
	}

	public void setEscalaMinistros(String escalaMinistros) {
		this.escalaMinistros = escalaMinistros;
	}
		
}