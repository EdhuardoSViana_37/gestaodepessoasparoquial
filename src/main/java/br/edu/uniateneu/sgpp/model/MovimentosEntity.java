package br.edu.uniateneu.sgpp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_Movimentos")

public class MovimentosEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Movimento")
	long idMovimento;

	@Column(name = "nomeMovimento")
	private String nome;

	@Column(name = "diaReuniao_Movimento")
	private String diaReunioes;

	@Column(name = "horarioReuniao_Movimento")
	private String horarioReunioes;

	@Column(name = "nomeCoordenador")
	private String nomeCoordenador;

	@Column(name = "atividadesRealizadas_Movimento")
	private String atividades;

	@ManyToOne
	@JoinColumn(name = "id_Pessoa")
	private PessoaEntity participante;
	
	public long getIdMovimento() {
		return idMovimento;
	}

	public void setIdMovimento(long idMovimento) {
		this.idMovimento = idMovimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDiaReunioes() {
		return diaReunioes;
	}

	public void setDiaReunioes(String diaReunioes) {
		this.diaReunioes = diaReunioes;
	}

	public String getHorarioReunioes() {
		return horarioReunioes;
	}

	public void setHorarioReunioes(String horarioReunioes) {
		this.horarioReunioes = horarioReunioes;
	}

	public String getNomeCoordenador() {
		return nomeCoordenador;
	}

	public void setNomeCoordenador(String nomeCoordenador) {
		this.nomeCoordenador = nomeCoordenador;
	}

	public String getAtividades() {
		return atividades;
	}

	public void setAtividades(String atividades) {
		this.atividades = atividades;
	}
}