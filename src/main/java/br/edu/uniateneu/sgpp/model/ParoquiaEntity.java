package br.edu.uniateneu.sgpp.model;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_Paroquia")

public class ParoquiaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Paroquia")
	long idParoquia;

	@Column(name = "nome_Paroquia")
	private String nome;

	@Column(name = "end_Paroquia")
	private String endereco;

	@Column(name = "tel_Paroquia")
	private String telefone;

	@Column(name = "mail_Paroquia")
	private String email;

	@OneToMany(mappedBy = "paroquia")
	private List<ReligiosoEntity> religiosos;
	
	public long getIdParoquia() {
		return idParoquia;
	}

	public void setIdParoquia(long idParoquia) {
		this.idParoquia = idParoquia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}